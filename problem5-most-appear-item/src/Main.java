import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] arrays = {"js", "js", "golang", "ruby", "ruby", "js", "js"};
        printResult(selectionSort(countItem(convertArrayToList(arrays))));
    }

    public static Map<String, Integer> selectionSort(Map<String, Integer> elements) {
        List<String> list = new ArrayList<>();
        for (String key: elements.keySet()) {
            list.add(key);
        };

        List<Integer> values = getValues(elements);

        // Selection sort
        int n = values.size();
        int temp = 0;
        for (int k = 0; k<n; k++) {
            int minimal = k;
            for (int j= k+1; j<n; j++) {
                if (values.get(j) < values.get(minimal)) {
                    minimal = j;
                }
            }
            temp = values.get(k);
            values.set(k, values.get(minimal));
            values.set(minimal, temp);
        }

        int i = 0, j=0;

        Map<String, Integer> map = new LinkedHashMap<>();

        for (i = 0; i<list.size(); i++) {
            for (j = 0; j< list.size(); j++) {
                if (values.get(i) == elements.get(list.get(j))) {
                    map.put(list.get(j), values.get(i));
                }
            }
        }
        return map;
    }

    public static List<Integer> getValues (Map<String, Integer> map) {
        List<Integer> list = new ArrayList<>();
        for (String key: map.keySet()) {
            list.add(map.get(key));
        }
        return list;
    }

    public static List<String> convertArrayToList(String[] arrays) {
            List<String> list = new ArrayList<>();
        for (int i = 0; i<arrays.length; i++) {
            list.add(arrays[i]);
        }
        return list;
    }

    public static Map<String, Integer> countItem(List<String> items) {
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i<items.size(); i++) {
            if (map.containsKey(items.get(i))) {
                map.put(items.get(i), map.get(items.get(i))+1);
            } else {
                map.put(items.get(i), 1);
            }
        }
        return map;
    }

    public static void printResult(Map<String, Integer> map) {
        for (String key: map.keySet()) {
            System.out.printf("%s -> %d", key, map.get(key));
            System.out.println();
        }
    }

}